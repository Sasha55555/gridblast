# Makefile for gridblast
# Compiles the code for the game
# Created on 6/18/2017
# Created by Andrew Davis
#
# Copyright (C) 2018  Andrew Davis
#
# This program is free software: you can redistribute it and/or modify   
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# define the compiler
CXX=g++

# define the compiler flags
CXXFLAGS=-c -Wall -std=c++14

# define the linker flags
LDFLAGS=-lcppsdl -lm

# define state-specific compiler flags
debug: CXXFLAGS += -g

# retrieve the source code
MAIN=$(shell ls src/*.cpp)

# list the source code
SOURCES=$(MAIN)

# compile the source code
OBJECTS=$(SOURCES:.cpp=.o)

# define the executable name
EXECUTABLE=gridblast

# start of build code

# target to compile the entire project without debug symbols
all: $(SOURCES) $(EXECUTABLE)

# target to build the executable without debug symbols
$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(OBJECTS) -o $@ $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $@ bin/
	mv -f $(OBJECTS) obj/

# target to build the executable with debug symbols
debug: $(OBJECTS)
	$(CXX) $(OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $(EXECUTABLE) bin/
	mv -f $(OBJECTS) obj/

# target to compile source code to object code
.cpp.o:
	$(CXX) $(CXXFLAGS) $< -o $@

# target to clean the workspace
clean:
	rm -rf bin
	rm -rf obj

# end of Makefile
