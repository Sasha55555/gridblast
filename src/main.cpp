/*
 * main.cpp
 * Main code file for gridblast
 * Created on 6/18/2017
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include <cstdlib>
#include <cppsdl/cppsdl.h>

//main function - main entry point for program
int main(int argc, char* argv[]) {
	//init cppsdl
	CPPSDL::Init();

	//log an info message
	CPPSDL::LogMessage("Gridblast is under construction.");
	CPPSDL::LogMessage("Check back later for more features.");

	//and return with no errors
	return EXIT_SUCCESS;
}

//end of program
